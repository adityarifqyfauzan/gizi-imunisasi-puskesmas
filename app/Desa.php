<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    
    protected $table = 'desa';

    protected $fillable = [
        'nama_desa', 'kecamatan_id'
    ];

    /**
     * Relasi antara table kecamatan dengan table desa
     * Setiap kecamatan punya banyak desa
     * 
     */
    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan');
    }

    /**
     * Relasi antara table desa dengan table keluarga
     * Setiap desa punya banyak keluarga
     * 
     */
    public function keluarga()
    {
        return $this->hasMany('App\Keluarga');
    }

    public function bidan()
    {
        return $this->hasOne('App\Bidan');
    }

}
