<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bidan extends Model
{
    protected $table = 'bidan';

    protected $fillable = [
        'nama_bidan', 'desa_id', 'user_id', 'alamat'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function desa()
    {
        return $this->belongsTo('App\Desa');
    }
}
