<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imunisasi extends Model
{
    protected $table = "imunisasi";

    protected $fillable = [
        'vaksin', 'bulan', 'keterangan'
    ];
}
