<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    protected $table = 'keluarga';

    protected $primaryKey = 'no_kk';
    public $incrementing = false;

    
    protected $fillable = [
        'no_kk', 'nik_ayah', 'nik_ibu', 'nama_ayah', 'nama_ibu', 'status_ekonomi', 'status_keluarga', 'no_hp', 'alamat', 'kecamatan_id', 'desa_id',
    ];

    /**
     * Relasi antara table kecamatan dengan table desa
     * Setiap kecamatan punya banyak desa
     * 
     */
    public function desa()
    {
        return $this->belongsTo('App\Desa');
    }

    /**
     * Relasi antara table kecamatan dengan table desa
     * Setiap kecamatan punya banyak desa
     * 
     */
    public function kecamatan()
    {
        return $this->belongsTo('App\Kecamatan');
    }

    public function anak()
    {
        return $this->hasMany('App\Anak');
    }

}
