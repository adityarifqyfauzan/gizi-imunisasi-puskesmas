<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Anak;
use App\Imunisasi;
use Auth;
use Alert;

class AnakController extends Controller
{
    public function store(Request $request)
    {
        $anak = new Anak;
        $anak->keluarga_no_kk = $request->keluarga_no_kk;
        $anak->nik_anak = $request->nik_anak;
        $anak->nama_anak = $request->nama_anak;
        $anak->tempat_lahir = $request->tempat_lahir;
        $anak->tgl_lahir = $request->tgl_lahir;
        $anak->bb_lahir = $request->bb_lahir;
        $anak->tb_lahir = $request->tb_lahir;
        $anak->kia = $request->kia;
        $anak->imd = $request->imd;

        $anak->save();

        $detailKeluarga = "/keluarga/detail/".$request->keluarga_no_kk;
        Alert::success('Tersimpan', 'Data Anak Berhasil Disimpan');
        return redirect($detailKeluarga);
    }

    public function show($id)
    {
        $anak = Anak::find($id);
        $page = "Anak";
        $umur = self::getUmur($anak->tgl_lahir);
        $vaksin = self::vaksinTime($umur['bulan']);

        return view('anak.index', ['vaksin' => $vaksin] , compact(['anak', 'umur', 'page']));
    }

    public function getUmur($tgl_lahir)
    {

        $date = \Carbon\Carbon::parse($tgl_lahir);
        $now = \Carbon\Carbon::now();

        $hari = $date->diffInDays($now);
        $bulan = $date->diffInMonths($now);
        $tahun = $date->diffInYears($now);

        

        return compact(['hari','bulan','tahun']);
    }

    public function vaksinTime($umur)
    {
        $vaksin = DB::table('imunisasi')
                    ->where('bulan', '=', $umur)
                    ->get('vaksin'); 
        
        // $vaksin = Imunisasi::where('bulan', $umur);
        return $vaksin;
    }
}
