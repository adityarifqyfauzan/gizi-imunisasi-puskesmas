<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kecamatan;
use App\Desa;
use Auth;
Use Alert;

class DesaController extends Controller
{
    public function index()
    {

        $user_id = Auth::user()->id;
        $kecamatan = DB::table('kecamatan')->find($user_id);
        $id_kecamatan = $kecamatan->id;
        $desa = Kecamatan::find($id_kecamatan)->desa;
        
        $page = 'Desa';
        
        return view('desa.index', compact(['desa', 'page']));
        
    }

    public function store(Request $request)
    {
        
        /**
         * Code ini digunakan untuk mencari kecamatan mana yang login kedalam sistem,
         * lalu setelah mengetahui siapa yang login maka id nya akan diambil.
         * Setelah mendapatkan id user, selanjutnya adalah cek data
         * yang ada didalam table kecamatan untuk mendapatkan
         * id kecamatan tersebut.
         * 
         */

        $user_id = Auth::user()->id;
        $kecamatan = DB::table('kecamatan')->find($user_id);
        $id_kecamatan = $kecamatan->id;
        
        $desa = new Desa;
        $desa->nama_desa = $request->nama_desa;
        $desa->kecamatan_id = $id_kecamatan;

        $desa->save();
        // toast('Your Post as been submited!','success');
        Alert::success('Tersimpan', 'Data Desa Berhasil Disimpan');
        return redirect('/desa');

    }

    public function update(Request $request, $id)
    {
        $desa = Desa::find($id);
        $desa_sebelum = $desa->nama_desa;
        $desa->nama_desa = $request->nama_desa;
        $desa->save();

        $message = "Desa ". $desa_sebelum . " diubah menjadi Desa ". $request->nama_desa;
        
        Alert::success('Updated', $message);
        return redirect('/desa');
    }

    public function destroy($id)
    {
        $desa = Desa::find($id);
        $desa_sebelum = $desa->nama_desa;
        
        try {
            //code...
            $desa->delete();
            $message = "Desa ". $desa_sebelum . " telah dihapus dari database";
    
            Alert::success('Data Terhapus', $message);
            return redirect('/desa');
        } catch (\Throwable $th) {
            //throw $th;
            
            echo "error";

        }

    }
    
}
