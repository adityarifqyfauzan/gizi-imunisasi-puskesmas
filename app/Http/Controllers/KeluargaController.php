<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Kecamatan;
use App\Keluarga;
use App\Desa;
use App\Anak;
use Validator;  
use Alert;


class KeluargaController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $kecamatan = DB::table('kecamatan')->find($user_id);
        $id_kecamatan = $kecamatan->id;
        $desa = Kecamatan::find($id_kecamatan)->desa;

        $keluarga = Kecamatan::find($id_kecamatan)->keluarga;
        $page = "Keluarga";
        
        return view('keluarga.index', compact(['keluarga', 'page', 'desa', 'kecamatan']));
    }

    public function store(Request $request)
    {
        
        $keluarga = new Keluarga;
        $keluarga->no_kk = $request->no_kk;
        $keluarga->nik_ayah = $request->nik_ayah;
        $keluarga->nik_ibu = $request->nik_ibu;
        $keluarga->nama_ayah = $request->nama_ayah;
        $keluarga->nama_ibu = $request->nama_ibu;
        $keluarga->status_ekonomi = $request->status_ekonomi;
        $keluarga->status_keluarga = $request->status_keluarga;
        $keluarga->no_hp = $request->no_hp;
        $keluarga->alamat = $request->alamat;
        $keluarga->kecamatan_id = $request->kecamatan_id;
        $keluarga->desa_id = $request->desa_id;

        $keluarga->save();
        $detailKeluarga = "/keluarga/detail/".$keluarga->no_kk;
        Alert::success('Tersimpan', 'Data Keluarga Berhasil Disimpan');
        return redirect($detailKeluarga);
        
    }

    public function update(Request $request, $id)
    {
        $keluarga = Keluarga::find($id);
        $keluarga->no_kk = $request->no_kk;
        $keluarga->nik_ayah = $request->nik_ayah;
        $keluarga->nik_ibu = $request->nik_ibu;
        $keluarga->nama_ayah = $request->nama_ayah;
        $keluarga->nama_ibu = $request->nama_ibu;
        $keluarga->status_ekonomi = $request->status_ekonomi;
        $keluarga->status_keluarga = $request->status_keluarga;
        $keluarga->no_hp = $request->no_hp;
        $keluarga->alamat = $request->alamat;
        $keluarga->kecamatan_id = $request->kecamatan_id;
        $keluarga->desa_id = $request->desa_id;

        $keluarga->save();
        $detailKeluarga = "/keluarga/detail/".$keluarga->no_kk;
        Alert::success('Updated', 'Data Keluarga Berhasil Diperbarui');
        return redirect($detailKeluarga);
    }

    public function detail($id)
    {
        $keluarga = Keluarga::find($id);
        $keluarga_desa_id = $keluarga->desa_id;
        $keluarga_kecamatan_id = $keluarga->kecamatan_id;
        // $desa = Desa::find($keluarga_desa_id)->desa;
        // $kecamatan = Kecamatan::find($keluarga_kecamatan_id)->kecamatan;
        $kecamatan = DB::table('kecamatan')->find($keluarga_kecamatan_id);
        $desa      = DB::table('desa')->find($keluarga_desa_id);
        $anak      = Keluarga::find($id)->anak;
        $listDesa  = Kecamatan::find($keluarga->kecamatan_id)->desa;

        $page = "Detail Keluarga";
        return view('keluarga.detail', compact(['anak', 'page', 'desa', 'kecamatan', 'keluarga', 'listDesa']));
    }
}
