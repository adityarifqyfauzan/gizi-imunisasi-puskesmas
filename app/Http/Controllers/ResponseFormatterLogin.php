<?php

namespace App\Http\Controllers;

class ResponseFormatterLogin
{
    protected static $response = [
        'status' => null,
        'message' => null,
        'data' => null
    ];

    public static function success($data = null, $message = null)
    {
        self::$response['status'] = true;
        self::$response['message'] = $message;
        self::$response['data'] = $data;

        return response()->json(self::$response);
    }

    public static function error($message = null)
    {
        self::$response['status'] = false;
        self::$response['message'] = $message;
        
        return response()->json(self::$response);
    }

}