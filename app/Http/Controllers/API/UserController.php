<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ResponseFormatterLogin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Bidan;
use Auth;

class UserController extends Controller
{
    // public function index()
    // {
    //     $listKeluarga = User::all();
        
    //     if ($listKeluarga) {
            
    //         $message = "Data Keluarga ditemukan";
    //         return ResponseFormatterLogin::success($listKeluarga, $message); 
    //         //return response()->json($listKeluarga);
    //     }else{

    //         $message = "Data Keluarga tidak ditemukan";
    //         $error = 404;
    //         return ResponseFormatterLogin::error($message, $error);

    //     }
    // }

    // public function show(Request $request)
    // {   
                
    // }

    public function bidanLogin(Request $request)
    {
        if (!$this->checkIfKodeExist($request->kode,$request->password)) {
            $message = "Kode yang Anda masukkan salah";
            return ResponseFormatterLogin::error($message);
        }

        if ($this->checkIfKodePasswordExist($request->kode,$request->password)) {
            $message = "Password yang Anda masukkan salah";
            return ResponseFormatterLogin::error($message);
        }

        $user = User::where(['kode' => $request->kode])->first();
        $bidan = Bidan::where(['user_id' => $user->id])->first();


        $message = "Data Bidan ditemukan";
        return ResponseFormatterLogin::success($user, $message);

    }

    public function checkifKodeExist($kode)
    {
        $user = User::where('kode', $kode)->first();
        
        if($user==null){
            return false;
        }else{
            
            return true;
        }
    }
    
    public function checkIfKodePasswordExist($kode, $password){
        $user = User::where('kode', $kode)->first();
        if (Hash::check($password, $user->password)) {
            return false;
        }
        else
            return true;
    }
}
