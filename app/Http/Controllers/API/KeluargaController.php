<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;
use App\Keluarga;

class KeluargaController extends Controller
{
    public function index()
    {
        $listKeluarga = Keluarga::all();
        
        if ($listKeluarga) {
            
            $message = "Data Keluarga ditemukan";
            return ResponseFormatter::success($listKeluarga, $message); 
            //return response()->json($listKeluarga);
        }else{

            $message = "Data Keluarga tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);

        }
    }

    public function show($id)
    {
        $keluarga = Keluarga::find($id);
        if ($keluarga) {

            $message = "Data keluarga ditemukan";
            return ResponseFormatter::success($keluarga, $message);
        
        }else{
            
            $message = "Data keluarga tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);
        
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'no_kk' => 'required',
            'nik_ayah' => 'required',
            'nik_ibu' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'status_ekonomi' => 'required',
            'status_keluarga' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required',
            'desa_id' => 'required',
            'kecamatan_id' => 'required',
        ];

        $message = [
            'required' => 'Silahkan isi :attribute, :attribute tidak boleh kosong',
        ];

        $data = collect($request)->all();

        $validator = Validator::make($data, $rules, $message);
        
        if ($validator->fails()) {
    
            $message = "Error Validation";
            $errors  = $validator->errors()->message();

            return ResponseFormatter::error($message, $error);
        
        }

        $keluarga = Keluarga::create($data);

        $message = "Data keluarga berhasil ditambahkan";

        return ResponseFormatter::success($keluarga, $message);

    }

    public function update(Request $request, $id)
    {
        $keluarga = Keluarga::find($id);
        $input = collect($request)->all();
        
        if ($keluarga) {
            
            $keluarga->update($input);
            $message = "Data keluarga telah diperbarui";
            return ResponseFormatter::success($keluarga, $message);

        }else{

            $message = "Data tidak ditemukan";
            return ResponseFormatter::error($message, 404);
            
        }
    }

    public function destroy($id)
    {
        # code...
    }
}
