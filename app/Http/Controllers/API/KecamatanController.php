<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;
use App\Kecamatan;

class KecamatanController extends Controller
{
    public function index()
    {
        $listKecamatan = Kecamatan::all();
        
        if ($listKecamatan) {
            
            $message = "Data Kecamatan ditemukan";
            // return ResponseFormatter::success($listKecamatan, $message); 
            return response()->json($listKecamatan);
        }else{

            $message = "Data Kecamatan tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);

        }
    }

    public function show($id)
    {
        $kecamatan = Kecamatan::find($id);
        if ($kecamatan) {

            $message = "Data Kecamatan ditemukan";
            return ResponseFormatter::success($kecamatan, $message);
        
        }else{
            
            $message = "Data Kecamatan tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);
        
        }
    }

    public function store(Request $request)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        
    }
}
