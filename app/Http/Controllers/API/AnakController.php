<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;
use App\Anak;

class AnakController extends Controller
{
    public function index()
    {
        $listAnak = Anak::all();
        
        if ($listAnak) {
            
            $message = "Data Anak ditemukan";
            // return ResponseFormatter::success($listAnak, $message); 
            return response()->json($listAnak);
        }else{

            $message = "Data Anak tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);

        }
    }

    public function show($id)
    {
        $anak = Anak::find($id);
        if ($anak) {

            $message = "Data anak ditemukan";
            return ResponseFormatter::success($anak, $message);
        
        }else{
            
            $message = "Data anak tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);
        
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'keluarga_no_kk' => 'required',
            'nik_anak' => 'required',
            'nama_anak' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'bb_lahir' => 'required',
            'tb_lahir' => 'required',
            'kia' => 'required',
            'imd' => 'required',
        ];

        $message = [
            'required' => 'Silahkan isi :attribute, :attribute tidak boleh kosong',
        ];

        $data = collect($request)->all();

        $validator = Validator::make($data, $rules, $message);
        
        if ($validator->fails()) {
    
            $message = "Error Validation";
            $errors  = $validator->errors()->message();

            return ResponseFormatter::error($message, $error);
        
        }

        $anak = Anak::create($data);

        $message = "Data anak berhasil ditambahkan";

        return ResponseFormatter::success($anak, $message);

    }

    public function update(Request $request, $id)
    {
        $anak = Anak::find($id);
        $input = collect($request)->all();
        
        if ($anak) {
            
            $anak->update($input);
            $message = "Data anak telah diperbarui";
            return ResponseFormatter::success($anak, $message);

        }else{

            $message = "Data tidak ditemukan";
            return ResponseFormatter::error($message, 404);
            
        }
    }

    public function destroy($id)
    {
        $anak = Anak::find($id);

        if($anak){
            
            $message = "Data anak telah berhasil dihapus";
            $anak->delete();
            return ResponseFormatter::success($anak, $message); 
            
        }else{

            $message = "Data tidak ditemukan";
            return ResponseFormatter::error($message, 404);
            
        }
    }
}
