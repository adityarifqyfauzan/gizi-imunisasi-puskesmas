<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;
use App\Desa;

class DesaController extends Controller
{
    public function index()
    {
        $listDesa = Desa::all();
        
        if ($listDesa) {
            
            $message = "Data Desa ditemukan";
            return ResponseFormatter::success($listDesa, $message); 
            //return response()->json($listDesa);
        }else{

            $message = "Data Desa tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);

        }
    }

    public function show($id)
    {
        $desa = Desa::find($id);
        if ($desa) {

            $message = "Data desa ditemukan";
            return ResponseFormatter::success($desa, $message);
        
        }else{
            
            $message = "Data desa tidak ditemukan";
            $error = 404;
            return ResponseFormatter::error($message, $error);
        
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'kecamatan_id' => 'required',
            'nama_desa' => 'required',
        ];

        $message = [
            'required' => 'Silahkan isi :attribute, :attribute tidak boleh kosong',
        ];

        $data = collect($request)->all();

        $validator = Validator::make($data, $rules, $message);
        
        if ($validator->fails()) {
    
            $message = "Error Validation";
            $errors  = $validator->errors()->message();

            return ResponseFormatter::error($message, $error);
        
        }

        $desa = Desa::create($data);

        $message = "Data desa berhasil ditambahkan";

        return ResponseFormatter::success($desa, $message);

    }

    public function update(Request $request, $id)
    {
        # code...
    }

    public function destroy($id)
    {
        # code...
    }

}
