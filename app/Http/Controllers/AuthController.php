<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Auth;
use Alert;

class AuthController extends Controller
{
    public function login()
    {
        return view('Auth.login');
    }

    public function postLogin(Request $request)
    {
        if (Auth::attempt($request->only('kode','password'))) {
            
            $user_id = Auth::user()->id;
            $kecamatan = DB::table('kecamatan')->find($user_id);
            $message = 'Anda berhasil Login sebagai Puskesmas '.$kecamatan->kecamatan;
            Alert::success('Selamat Datang', $message);
            
            return redirect('/dashboard');
        
        }
        
        return redirect('/login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function dashboard()
    {
        $user_id = Auth::user()->id;
        $kecamatan = DB::table('kecamatan')->find($user_id);
        $page = "Dashboard";

        // $message = 'Anda berhasil Login dengan sebagai Puskesmas '.$kecamatan->kecamatan;
        // Alert::success('Selamat Datang', $message);

        return view('dashboard.index', compact('page'));
    }
}
