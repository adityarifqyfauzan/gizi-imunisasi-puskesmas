<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';
    
    protected $fillable = [
        'kecamatan', 'user_id'
    ];

    /**
     * Relasi antara table user dengan table kecamatan
     * Setiap satu kecamatan punya satu akun user
     *  
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relasi antara table kecamatan dengan table desa
     * Setiap kecamatan punya banyak desa
     * 
     */
    public function desa()
    {
        return $this->hasMany('App\Desa');
    }

    /**
     * Relasi antara table kecamatan dengan table keluarga
     * Setiap kecamatan punya banyak keluarga
     * 
     */
    public function keluarga()
    {
        return $this->hasMany('App\Keluarga');
    }


}
