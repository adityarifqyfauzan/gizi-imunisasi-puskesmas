<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anak extends Model
{
    protected $table = 'anak';

    protected $fillable = [
        'keluarga_no_kk', 'nik_anak', 'nama_anak', 'tempat_lahir', 'tgl_lahir', 'bb_lahir', 'tb_lahir', 'kia', 'imd'
    ];

    public function keluarga()
    {
        return $this->belongsTo('App\Keluarga');
    }

    
}
