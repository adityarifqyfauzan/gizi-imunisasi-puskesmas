@extends('template.default')

@section('content')

<div class="card col-12">
    <div class="card-header">
      <h3 class="card-title">Data Keluarga</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#modalAdd">
        Tambah Data
      </button>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="20">#</th>
          <th>No. Kartu Keluarga</th>
          <th>Nama Ayah</th>
          <th>Nama Ibu</th>
          <th width="150" class="text-center">Aksi</th>
        </tr>
        </thead>

        <tfoot>
          <tr>
            <th width="20">#</th>
            <th>No. Kartu Keluarga</th>
            <th>Nama Ayah</th>
            <th>Nama Ibu</th>
            <th width="150" class="text-center">Aksi</th>
          </tr>
        </tfoot>

        <tbody>
          @if(count($keluarga) != 0)
          
            @foreach ($keluarga as $data)
                <tr>
                  <td class="text-center">{{ $loop->iteration }}</td>
                  <td>{{ $data->no_kk }}</td>
                  <td>{{ $data->nama_ayah }}</td>
                  <td>{{ $data->nama_ibu }}</td>
                  <td  class="text-center">
                    <a href="/keluarga/detail/{{ $data->no_kk }}" class="btn btn-outline-primary">Lihat Detail</a>
                  </td>
                </tr>
            @endforeach
          
          @else
                <tr>
                  <td colspan="5">Belum ada data Desa</td>
                </tr>
          @endif
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->

    <!-- Modal Add Data-->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalAddLabel">Menambahkan Data Keluarga</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/keluarga" method="POST">
          <div class="modal-body">
              {{ csrf_field() }}
              {{ method_field('POST') }}

              <div class="form-group">
                <label>Nomor Kartu Keluarga</label>
                <input type="text" class="form-control" name="no_kk" placeholder="Cth : 3212223334567889" required>
                <small class="form-text text-muted">Nomor KK terdiri dari 16 angka</small>
              </div>
              
              <div class="form-group">
                <label>NIK Ayah</label>
                <input type="text" class="form-control" name="nik_ayah" placeholder="" required>
                <small class="form-text text-muted">Nomor NIK Ayah terdiri dari 16 angka</small>
              </div>
              
              <div class="form-group">
                <label>NIK Ibu</label>
                <input type="text" class="form-control" name="nik_ibu" placeholder="" required>
                <small class="form-text text-muted">Nomor NIK Ibu terdiri dari 16 angka</small>
              </div>
              
              <div class="form-group">
                <label>Nama Ayah</label>
                <input type="text" class="form-control" name="nama_ayah" placeholder="" required>
                <small class="form-text text-muted">Nama Ayah harus diawali dengan huruf kapital</small>
              </div>
              
              <div class="form-group">
                <label>Nama Ibu</label>
                <input type="text" class="form-control" name="nama_ibu" placeholder="" required>
                <small class="form-text text-muted">Nama Ibu harus diawali dengan huruf kapital</small>
              </div>
              
              <div class="form-group">
                <label>Status Keluarga</label>
                <input type="text" class="form-control" name="status_keluarga" placeholder="" required>
              </div>
              
              <div class="form-group">
                <label>Status Ekonomi</label>
                <input type="text" class="form-control" name="status_ekonomi" placeholder="" required>
              </div>
              
              <div class="form-group">
                <label>Nomor Handphone</label>
                <input type="text" class="form-control" name="no_hp" placeholder="" required>
              </div>

              <div class="form-group">
                <label>Alamat</label>
                <textarea name="alamat"cols="30" class="form-control" rows="10"></textarea>
              </div>
            
              <div class="form-group">
                <label>Desa</label>
                <select name="desa_id" id="" class="form-control">
                  @foreach ($desa as $dataDesa)
                  
                  <option value="{{ $dataDesa->id }}"> {{ $dataDesa->nama_desa }}</option>
                      
                  @endforeach
                </select>
              </div>
              
              <div class="form-group">
                <label>Kecamatan</label>
                <input type="hidden" class="form-control" name="kecamatan_id" value="{{ $kecamatan->id }}" placeholder="Cth : Sukamaju" required>
                <input type="text" class="form-control" value="{{ $kecamatan->kecamatan }}" readonly placeholder="" required>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Edit Data-->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalDeleteLabel">Hapus Data Desa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/desa" method="POST" id="deleteForm">
          <div class="modal-body">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
            
              <input type="hidden" name="_method" value="DELETE">
              <p class="text-center">Apakah Anda yakin ingin menghapus <span id="desaNama"></span> ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Tidak</button>
            <button type="submit" class="btn btn-danger">Ya, Hapus Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>

    
@endsection

@section('js-section')
<script type="text/javascript">

  $(document).ready(function () {  

    var table = $("#example1").DataTable();

    //start edit data
    table.on('click', '.edit', function () {  
      $tr = $(this).closest('tr');

      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      console.log(data);

      $('#nama_desa').val(data[2]);

      $('#editForm').attr('action', '/desa/'+data[0]);
      $('#modalEdit').modal('show');  
    });

    //delete data
    table.on('click', '.hapus', function () {  
      $tr = $(this).closest('tr');

      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      console.log(data);

      $('#deleteForm').attr('action', '/desa/'+data[0]);
      $('#modalDelete').modal('show');
    
    });

  });

</script>
@endsection