@extends('template.default')

@section('content')

<div class="card col-12">
    
    <div class="card-body">
      
      <div class="card card-primary">

        <div class="card-header">
          <h3 class="card-title">Detail Data Orang Tua</h3>
        </div>

        <div class="card-body">
          
          <div class="row pb-3">
            <div class="col-lg-12">
              <a href="/keluarga" class="btn btn-outline-info">Kembali</a>
              <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modalEdit">
                <i class="fas fa-pen-square"></i>
              </button>
              <a href="#" class="btn btn-outline-danger hapus"><i class="fas fa-trash-alt"></i></a>
            </div>
          </div>
          
          <div class="row">
            
          <div class="col-lg-6">
            <table class="table table-bordered">
              <tr>
                
                <th width="200">No Kartu Keluarga</th>
                <th width="20">:</th>
                <td>{{ $keluarga->no_kk }}</td>
                
              </tr>
              <tr>
                
                <th>NIK Ayah</th>
                <th>:</th>
                <td>{{ $keluarga->nik_ayah }}</td>
                
              </tr>
              <tr>
                
                <th>NIK Ibu</th>
                <th>:</th>
                <td>{{ $keluarga->nik_ibu }}</td>
                
              </tr>
              <tr>
                
                <th>Nama Ayah</th>
                <th>:</th>
                <td>{{ $keluarga->nama_ayah }}</td>
                
              </tr>
              <tr>
                
                <th>Nama Ibu</th>
                <th>:</th>
                <td>{{ $keluarga->nama_ibu }}</td>
                
              </tr>
            </table>
          </div>
          
          <div class="col-lg-6">
            <table class="table table-bordered">
              <tr>
                
                <th width="200">Status Ekonomi</th>
                <th width="20">:</th>
                <td>{{ $keluarga->status_ekonomi }}</td>
                
              </tr>
              <tr>
                
                <th>Status Keluarga</th>
                <th>:</th>
                <td>{{ $keluarga->status_keluarga }}</td>
                
              </tr>
              <tr>
                
                <th>Nomor Handphone</th>
                <th>:</th>
                <td>{{ $keluarga->no_hp }}</td>
                
              </tr>
              <tr>
                
                <th>Alamat</th>
                <th>:</th>
                <td>{{ $keluarga->alamat }}</td>
                
              </tr>
              <tr>
                
                <th>Desa</th>
                <th>:</th>
                <td>{{ $desa->nama_desa }}</td>
                
              </tr>
              <tr>
                
                <th>Kecamatan</th>
                <th>:</th>
                <td>{{ $kecamatan->kecamatan }}</td>
                
              </tr>
            </table>
          </div>
        </div>

        </div>
    
      </div>

      <div class="card card-primary">

        <div class="card-header">
          <h3 class="card-title">Detail Data Anak</h3>
        </div>
        <div class="card-body">
          <div class="row mb-3">
            <div class="col-lg-12">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddAnak">
                Tambah Data
              </button>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-12">

              <table id="example1" class="table table-bordered table-striped table-hover table-sm">
                <thead>
                  <tr>

                    <th class="text-center align-middle" width="100">NIK</th>
                    <th class="text-center align-middle">Nama</th>
                    <th class="text-center align-middle">Tempat, Tgl Lahir</th>
                    <th class="text-center align-middle" width="120">Aksi</th>

                  </tr>

                </thead>
                
                <tfoot>
                  <tr>
                    <th class="text-center align-middle">NIK</th>
                    <th class="text-center align-middle">Nama</th>
                    <th class="text-center align-middle">Tempat, Tgl Lahir</th>
                    <th class="text-center align-middle">Aksi</th>
                  
                  </tr>

                </tfoot>

                <tbody>
                  @if (count($anak) != 0)
                      @foreach ($anak as $dataAnak)
                        
                        <tr>
                          <td class="text-center">{{ $dataAnak->nik_anak }}</td>
                          <td class="text-center">{{ $dataAnak->nama_anak }}</td>
                          <td class="text-center">{{ $dataAnak->tempat_lahir }}, {{ $dataAnak->tgl_lahir }}</td>
                          <td class="text-center">
                            <a href="/anak/{{ $dataAnak->id }}" class="btn btn-outline-info btn-sm">Detail</a>
                          </td>
                        </tr>

                      @endforeach
                  @else
                      <tr>
                        <td colspan="8" class="text-center"> Belum Ada Data</td>
                      </tr>
                  @endif
                
                </tbody>
              </table>
            
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <!-- /.card-body -->

    <!-- Modal Edit Data Keluarga-->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalEditLabel">Memperbarui Data Keluarga</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/keluarga/{{ $keluarga->no_kk }}" method="POST" id="editForm">
          <div class="modal-body">
              {{ csrf_field() }}
              {{ method_field('PUT') }}

              <div class="form-group">
                <label>Nomor Kartu Keluarga</label>
                <input type="text" class="form-control" name="no_kk" placeholder="Cth : 3212223334567889" value="{{ $keluarga->no_kk }}" required>
                <small class="form-text text-muted">Nomor KK terdiri dari 16 angka</small>
              </div>
              
              <div class="form-group">
                <label>NIK Ayah</label>
                <input type="text" class="form-control" name="nik_ayah" placeholder="" value="{{ $keluarga->nik_ayah }}" required>
                <small class="form-text text-muted">Nomor NIK Ayah terdiri dari 16 angka</small>
              </div>
              
              <div class="form-group">
                <label>NIK Ibu</label>
                <input type="text" class="form-control" name="nik_ibu" placeholder="" value="{{ $keluarga->nik_ibu }}" required>
                <small class="form-text text-muted">Nomor NIK Ibu terdiri dari 16 angka</small>
              </div>
              
              <div class="form-group">
                <label>Nama Ayah</label>
                <input type="text" class="form-control" name="nama_ayah" placeholder="" value="{{ $keluarga->nama_ayah }}" required>
                <small class="form-text text-muted">Nama Ayah harus diawali dengan huruf kapital</small>
              </div>
              
              <div class="form-group">
                <label>Nama Ibu</label>
                <input type="text" class="form-control" name="nama_ibu" placeholder="" value="{{ $keluarga->nama_ibu }}" required>
                <small class="form-text text-muted">Nama Ibu harus diawali dengan huruf kapital</small>
              </div>
              
              <div class="form-group">
                <label>Status Keluarga</label>
                <input type="text" class="form-control" name="status_keluarga" placeholder="" value="{{ $keluarga->status_keluarga }}" required>
              </div>
              
              <div class="form-group">
                <label>Status Ekonomi</label>
                <input type="text" class="form-control" name="status_ekonomi" placeholder="" value="{{ $keluarga->status_ekonomi }}" required>
              </div>
              
              <div class="form-group">
                <label>Nomor Handphone</label>
                <input type="text" class="form-control" name="no_hp" placeholder="" value="{{ $keluarga->no_hp }}" required>
              </div>

              <div class="form-group">
                <label>Alamat</label>
                <textarea name="alamat"cols="30" class="form-control" rows="10">{{ $keluarga->alamat }}</textarea>
              </div>
            
              <div class="form-group">
                <label>Desa</label>
                <select name="desa_id" id="" class="form-control">
                  @foreach ($listDesa as $dataDesa)

                  @if ($dataDesa->nama_desa == $desa->nama_desa)
                    
                    <option value="{{ $dataDesa->id }}" selected> {{ $dataDesa->nama_desa }}</option>
                    
                  @else
                    
                    <option value="{{ $dataDesa->id }}"> {{ $dataDesa->nama_desa }}</option>
                  
                  @endif
                      
                  @endforeach
                </select>
              </div>
              
              <div class="form-group">
                <label>Kecamatan</label>
                <input type="hidden" class="form-control" name="kecamatan_id" value="{{ $kecamatan->id }}" placeholder="Cth : Sukamaju" required>
                <input type="text" class="form-control" value="{{ $kecamatan->kecamatan }}" readonly placeholder="" required>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Perbarui Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Add Data Anak-->
    <div class="modal fade" id="modalAddAnak" tabindex="-1" role="dialog" aria-labelledby="modalAddAnakLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalAddAnakLabel">Menambahkan Data Anak</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/anak" method="POST">
          <div class="modal-body">
              {{ csrf_field() }}
              {{ method_field('POST') }}

              <div class="form-group">
                <input type="hidden" class="form-control" name="keluarga_no_kk" value="{{ $keluarga->no_kk }}" required>
              </div>
              
              <div class="form-group">
                <label>NIK Anak</label>
                <input type="text" class="form-control" name="nik_anak" placeholder="" required>
                <small class="form-text text-muted">Nomor NIK Anak terdiri dari 16 angka</small>
              </div>
              
              <div class="form-group">
                <label>Nama Anak</label>
                <input type="text" class="form-control" name="nama_anak" placeholder="" required>
                <small class="form-text text-muted">Nama Anak harus diawali dengan huruf kapital</small>
              </div>
              
              <div class="form-group">
                <label>Tempat Lahir</label>
                <input type="text" class="form-control" name="tempat_lahir" placeholder="Cth : Indramayu" required>
                <small class="form-text text-muted">Tempat harus diawali dengan huruf kapital</small>
              </div>
              
              <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" required>
              </div>
              
              <div class="form-group">
                <label>Berat Badan Ketika Lahir</label>
                <input type="text" maxlength="5" class="form-control" name="bb_lahir" placeholder="Cth : 3.4" required>
                <small class="form-text text-muted">Isi dengan angka, Untuk satuan berat badan adalah Kilogram</small>
              </div>
              
              <div class="form-group">
                <label>Tinggi Badan</label>
                <input type="text" max="5" class="form-control" name="tb_lahir" placeholder="Cth : 75" required>
                <small class="form-text text-muted">Isi dengan angka, Untuk satuan tinggi badan adalah Centimeter</small>
              </div>

              <div class="form-group">
                <label>KIA</label>
                <input type="text" class="form-control" name="kia" required>
              </div>
            
              <div class="form-group">
                <label>IMD</label>
                <select name="imd" id="" class="form-control">
              
                  <option value="Ya">Ya</option>
                  <option value="Tidak">Tidak</option>
                      
                </select>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>

@endsection