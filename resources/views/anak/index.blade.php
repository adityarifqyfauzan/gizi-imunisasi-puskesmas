@extends('template.default')

@section('content')

<div class="card col-12">
    
    <div class="card-body">
      
      <div class="row">

        <div class="col-12">

          <div class="card card-warning">
    
            <div class="card-header">
              <h3 class="card-title">Detail Data Anak</h3>
            </div>
    
            <div class="card-body">
              
              <div class="row pb-3">
                <div class="col-lg-12">
                  <a href="/keluarga" class="btn btn-outline-info">Kembali</a>
                  <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modalEdit">
                    <i class="fas fa-pen-square"></i>
                  </button>
                  <a href="#" class="btn btn-outline-danger hapus"><i class="fas fa-trash-alt"></i></a>
                </div>
              </div>
              
              <div class="row">
                
              <div class="col-lg-6">
                <table class="table table-bordered">
                  <tr>
                    
                    <th width="200">No Induk Keluarga</th>
                    <th width="20">:</th>
                    <td>{{ $anak->nik_anak }}</td>
                    
                  </tr>
                  <tr>
                    
                    <th>Nama Anak</th>
                    <th>:</th>
                    <td>{{ $anak->nama_anak }}</td>
                    
                  </tr>
                  <tr>
                    
                    <th>Tempat Lahir</th>
                    <th>:</th>
                    <td>{{ $anak->tempat_lahir }}</td>
                    
                  </tr>
                  <tr>
                    
                    <th>Tanggal Lahir</th>
                    <th>:</th>
                    <td>{{ $anak->tgl_lahir }}</td>
                    
                  </tr>
                  <tr>
                    
                    <th>Umur</th>
                    <th>:</th>
                    <td> {{ $umur['tahun'] }} tahun, {{ $umur['bulan'] }} bulan</td>
                    
                  </tr>
                </table>
              </div>
              
              <div class="col-lg-6">
                <table class="table table-bordered">
                  <tr>
                    
                    <th width="200">KIA</th>
                    <th width="20">:</th>
                    <td>{{ $anak->kia }}</td>
                    
                  </tr>
                  <tr>
                    
                    <th>IMD</th>
                    <th>:</th>
                    <td>{{ $anak->imd }}</td>
                    
                  </tr>
                </table>
              </div>
            </div>
    
            </div>
        
          </div>
        
        </div>

      </div>

      <div class="row">

        <div class="col-4">

          <div class="card card-primary">
    
            <div class="card-header">
              <h3 class="card-title">Vaksin yang harus diberikan</h3>
            </div>
            <div class="card-body">
              @foreach ($vaksin as $dataVaksin)
                <span class="badge badge-pill badge-warning" style="font-size: 15px">{{ $dataVaksin->vaksin }}</span>
              @endforeach
            </div>
            
          </div>
        
        </div>
        
        <div class="col-8">
          
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Riwayat Vaksin</h3>
            </div>
            <div class="card-body">
              
              <table class="table">

              </table>

            </div>
            
          </div>
        
        </div>
        

      </div>
    </div>
    <!-- /.card-body -->

@endsection