@extends('template.default')

@section('content')

<div class="card col-12">
    <div class="card-header">
      <h3 class="card-title">Data Desa</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <button type="button" class="btn btn-primary mb-4" data-toggle="modal" data-target="#modalAdd">
        Tambah Data
      </button>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th hidden width="20">#</th>
          <th width="20">#</th>
          <th>Nama Desa</th>
          <th width="150" class="text-center">Aksi</th>
        </tr>
      </thead>

        <tbody>
          @if(count($desa) != 0)
          
            @foreach ($desa as $data)
                <tr>
                  <td hidden>{{ $data->id }}</td>
                  <td class="text-center">{{ $loop->iteration }}</td>
                  <td>{{ $data->nama_desa }}</td>
                  <td  class="text-center">
                    <a href="#" class="btn btn-outline-success edit"><i class="fas fa-pen-square"></i></a>
                    <a href="#" class="btn btn-outline-danger hapus"><i class="fas fa-trash-alt"></i></a>
                  </td>
                </tr>
            @endforeach
          
          @else
                <tr>
                  <td colspan="3">Belum ada data Desa</td>
                </tr>
          @endif
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->

    <!-- Modal Add Data-->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalAddLabel">Menambahkan Data Desa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/desa" method="POST">
          <div class="modal-body">
              {{ csrf_field() }}
              {{ method_field('POST') }}
              <div class="form-group">
                <label>Nama Desa</label>
                <input type="text" class="form-control" name="nama_desa" placeholder="Cth : Sukamaju" required>
                <small id="emailHelp" class="form-text text-muted">*Nama desa diawali dengan huruf kapital</small>
              </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal Edit Data-->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalEditLabel">Perbarui Data Desa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/desa" method="POST" id="editForm">
          <div class="modal-body">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="form-group">
                <label>Nama Desa</label>
                <input type="text" class="form-control" id="nama_desa" name="nama_desa" placeholder="Cth : Sukamaju" required>
                <small id="emailHelp" class="form-text text-muted">*Nama desa diawali dengan huruf kapital</small>
              </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Perbarui Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal Edit Data-->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalDeleteLabel">Hapus Data Desa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/desa" method="POST" id="deleteForm">
          <div class="modal-body">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
            
              <input type="hidden" name="_method" value="DELETE">
              <p class="text-center">Apakah Anda yakin ingin menghapus <span id="desaNama"></span> ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Tidak</button>
            <button type="submit" class="btn btn-danger">Ya, Hapus Data</button>
          </form>
          </div>
        </div>
      </div>
    </div>

    
@endsection

@section('js-section')
<script type="text/javascript">

  $(document).ready(function () {  

    var table = $("#example1").DataTable();

    //start edit data
    table.on('click', '.edit', function () {  
      $tr = $(this).closest('tr');

      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      console.log(data);

      $('#nama_desa').val(data[2]);

      $('#editForm').attr('action', '/desa/'+data[0]);
      $('#modalEdit').modal('show');  
    });

    //delete data
    table.on('click', '.hapus', function () {  
      $tr = $(this).closest('tr');

      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      console.log(data);

      $('#desaNama').text(data[2]);

      $('#deleteForm').attr('action', '/desa/'+data[0]);
      $('#modalDelete').modal('show');
    
    });

    
  });

</script>
@endsection