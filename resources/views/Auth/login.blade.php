<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Sipagi - Puskesmas | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="/"><b>Sipagi</b> - Puskesmas</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">

      <p class='text-center pt-3 pb-4'><img width='30%' class='rounded-circle img-thumbnail' src="{{ asset('assets/dist/img/hospital.png') }}"></p>
      {{-- <p class="login-box-msg pb-4">Masuk sebagai Puskesmas</p> --}}

      @if(session()->has('message'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert"">
            {{ session()->get('message') }}
          <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
      @endif

      <form action="{{ route('login') }}" method="POST">

        {{--  <input type="hidden" name="_method" value="POST">  --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <label class='mb-1'>Kode</label>
        <div class="input-group mb-4">
          <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-id-card-alt"></span>
            </div>
          </div>
        </div>
        
        @if ($errors->has('kode_puskesmas'))
            <span class="error">{{ $errors->first('kode_puskesmas') }}</span>
        @endif

        <label class='mb-1'>Password</label>
        <div class="input-group mb-4">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        
        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif 
        
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block mb-3 mt-2">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>

</body>
</html>
