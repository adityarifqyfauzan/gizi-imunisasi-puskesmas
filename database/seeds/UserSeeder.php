<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['kode' => 'PUS-LOH-0420-001',
            'password' => Hash::make('password'),
            'role' => 'puskesmas',
            'name' => 'Lohbener'],

            ['kode' => 'PUS-LOH-PAM0201',
            'password' => Hash::make('password_bidan'),
            'role' => 'bidan',
            'name' => 'Bidan Pamayahan']
        ]);
    }
}
