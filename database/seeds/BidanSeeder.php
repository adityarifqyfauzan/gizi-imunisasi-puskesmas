<?php

use Illuminate\Database\Seeder;

class BidanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bidan')->insert([
            'desa_id' => 1,
            'user_id' => 2,
            'nama_bidan' => 'Bidan Pamayahan',
            'alamat' => 'Desa Pamayahan',
        ]);
    }
}
