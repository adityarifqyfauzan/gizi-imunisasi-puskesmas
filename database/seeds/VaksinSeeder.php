<?php

use Illuminate\Database\Seeder;

class VaksinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imunisasi')->insert([
            [
                'vaksin' => 'HB0',
                'bulan'  => '0',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'BCG',
                'bulan'  => '1',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'POLIO 1',
                'bulan'  => '1',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'POLIO 2',
                'bulan'  => '2',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'POLIO 3',
                'bulan'  => '3',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'POLIO 4',
                'bulan'  => '4',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'IPV',
                'bulan'  => '4',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'DPT/HB/HIB 1',
                'bulan'  => '2',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'DPT/HB/HIB 2',
                'bulan'  => '3',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'DPT/HB/HIB 3',
                'bulan'  => '4',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'MR',
                'bulan'  => '9',
                'keterangan' => 'dasar' 
            ],
            [
                'vaksin' => 'DPT/HB/HIB',
                'bulan'  => '0',
                'keterangan' => 'booster' 
            ],
            [
                'vaksin' => 'CAMPAK',
                'bulan'  => '0',
                'keterangan' => 'booster' 
            ],
        ]);
    }
}
