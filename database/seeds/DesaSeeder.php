<?php

use Illuminate\Database\Seeder;

class DesaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('desa')->insert([
            'kecamatan_id' => 1,
            'nama_desa' => 'Pamayahan',
        ]);
    }
}
