<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desa', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->unsignedBigInteger('kecamatan_id');
            $table->string('nama_desa');
            $table->timestamps();

        });
        Schema::table('desa', function (Blueprint $table)
        {
            
            $table->foreign('kecamatan_id')
                    ->references('id')->on('kecamatan');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desa');
    }
}
