<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeluargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keluarga', function (Blueprint $table) {
            $table->string('no_kk')->primary();
            $table->string('nik_ayah');
            $table->string('nik_ibu')->nullable();
            $table->string('nama_ayah');
            $table->string('nama_ibu');
            $table->string('status_ekonomi');
            $table->string('status_keluarga');
            $table->string('no_hp', 13);
            $table->string('alamat');
            $table->unsignedBigInteger('kecamatan_id');
            $table->unsignedBigInteger('desa_id');
            $table->timestamps();
        });
        Schema::table('keluarga', function (Blueprint $table)
        {
            
            $table->foreign('kecamatan_id')
                    ->references('id')->on('kecamatan')
                    ->onDelete('cascade');
            // $table->foreign('desa_id')
            //         ->references('id')->on('desa')
            //         ->onDelete('cascade');
        
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keluarga');
    }
}
