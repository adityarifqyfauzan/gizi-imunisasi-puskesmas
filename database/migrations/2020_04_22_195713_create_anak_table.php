<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anak', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('keluarga_no_kk')->index();
            $table->string('nik_anak')->unique();
            $table->string('nama_anak');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->integer('bb_lahir');
            $table->integer('tb_lahir');
            $table->string('kia');
            $table->enum('imd', ['Ya', 'Tidak'])->default('Ya');
            $table->timestamps();

            $table->foreign('keluarga_no_kk')
                    ->references('no_kk')->on('keluarga');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anak');
    }
}
