<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bidan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('desa_id');
            $table->unsignedBigInteger('user_id');
            $table->string('nama_bidan');
            $table->string('alamat');
            $table->timestamps();

            $table->foreign('desa_id')
                    ->references('id')->on('desa');

            $table->foreign('user_id')
                    ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bidan');
    }
}
