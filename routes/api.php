<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'anak'], function () {
    Route::post('/', 'API\AnakController@store');
    Route::get('/', 'API\AnakController@index');
    Route::get('/{id}', 'API\AnakController@Show');
    Route::patch('/{id}', 'API\AnakController@update');
    Route::delete('/{id}', 'API\AnakController@destroy');
});

Route::group(['prefix' => 'keluarga'], function () {
    Route::post('/', 'API\KeluargaController@store');
    Route::get('/', 'API\KeluargaController@index');
    Route::get('/{id}', 'API\KeluargaController@Show');
    Route::patch('/{id}', 'API\KeluargaController@update');
    Route::delete('/{id}', 'API\KeluargaController@destroy');
});

Route::group(['prefix' => 'desa'], function () {
    Route::get('/', 'API\DesaController@index');
    Route::get('/{id}', 'API\DesaController@Show');;
});

Route::post('user', 'API\UserController@bidanLogin');

Route::group(['prefix' => 'kecamatan'], function () {
    Route::get('/', 'API\KecamatanController@index');
    Route::get('/{id}', 'API\KecamatanController@Show');
});