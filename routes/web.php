<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'guest'], function () {
    
    Route::get('login', 'AuthController@login')->name('login-form');
    Route::post('postlogin', 'AuthController@postLogin')->name('login'); 
    
});

Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::get('/dashboard', 'AuthController@dashboard');
    
    Route::resource('/desa', 'DesaController');
    Route::resource('/keluarga', 'KeluargaController');
    Route::resource('/anak', 'AnakController');
    Route::resource('/bidan', 'BidanController');
    Route::get('/keluarga/detail/{id}', 'KeluargaController@detail');
    
    Route::get('/anak/{id}', 'AnakController@show');
});